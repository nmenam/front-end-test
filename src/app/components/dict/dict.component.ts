import { Component } from '@angular/core';
import { ApiService } from "../../services/api.service";

@Component({
  selector: 'app-dict',
  templateUrl: './dict.component.html',
  styles: []
})
export class DictComponent {

  success: boolean;
  status: string;
  alphabet: string[] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o',
                 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  paragraphs: {paragraph: string, number: number, hasCopyright: boolean}[];
  constructor(private _apiService: ApiService) {
    this.status = 'component-idle';
   }

  getDataFromAPI(){
    this.success = false;
    this.status = 'fetching-data';
    this._apiService.getData('dict')
                      .subscribe((data: any) => {
                        this.status = 'response-loaded';
                        if (data.data != null) {
                          this.paragraphs = data.data;
                          this.success = data.success;
                        } else {
                          this.success = data.success;
                        }
                        console.log(data.data);
                      });
  }

  countChars(paragraph: string, char: string) {
    let qty  = 0;
    for ( let i = 0; i < paragraph.length; i++) {
      const letter = paragraph.substring(i, ( i + 1 ));
      if( letter === char) {
        qty++;
      }
    }
    return qty;
  }

  countNumbers(paragraph: string) {
    let qty  = 0;
    const numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    for ( let i = 0; i < paragraph.length; i++) {
      const letter = paragraph.substring(i, ( i + 1 ));
      if ( numbers.includes(letter) ) {
        qty++;
      }
    }
    return qty;
  }

}
