import { Component } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styles: []
})
export class ArrayComponent {


  status: string;
  values: number[] = [];
  uniqueValues: number[];
  success: boolean;

  constructor( private _apiService: ApiService ) {
    this.status = 'component-idle';
  }

  getDataFromAPI(){
    this.values = [];
    this.success = false;
    this.uniqueValues = [];
    this.status = 'fetching-data';
    this._apiService.getData('array')
                      .subscribe((data: any) => {
                        this.status = 'response-loaded';
                        if (data.data != null) {
                          this.success = data.success;
                          this.values = data.data;
                          this.uniqueValues = [...new Set(this.values)];
                        } else {
                          this.success = data.success;
                          this.values = null;
                          this.uniqueValues = null;
                        }
                      });
  }
  numberQty(numberToCount: number) {
    const qty = this.values.filter(filteredNumber => filteredNumber === numberToCount).length;
    return qty;
  }

  firstPos(numberToFind: number) {
    return this.values.findIndex((element) => element === numberToFind);
  }

  lastPos(numberToFind: number) {
    return this.values.lastIndexOf( numberToFind);
  }

  }




interface IRemoteResponse{
  data?: any;
  error?: any;
  success: boolean;
}