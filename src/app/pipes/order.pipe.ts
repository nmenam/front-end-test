import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'orderNumValues'
})

export class OrderPipe implements PipeTransform {
    transform(values: number[] , method: string ): any {

        if (!values) {
            return;
        }

        const  calculateValues = [...values];
        
       
        if (method === 'asc') {
            let done = false;
            while (!done) {
                done = true;
                for (let i = 1 ; i < calculateValues.length; i++) {
                    if ( calculateValues[i - 1 ] > calculateValues[i] ) {
                        done = false;
                        const tmp = calculateValues[ i - 1 ];
                        calculateValues[ i - 1 ] = calculateValues[i];
                        calculateValues[i] = tmp;

                    }
                }
            }
            return calculateValues;
        } else {
            let done = false;
            while (!done) {
                done = true;
                for (let i = 1 ; i < calculateValues.length; i++) {
                    if ( calculateValues[i -1] < calculateValues[i] ) {
                        done = false;
                        const tmp = calculateValues[ i - 1 ];
                        calculateValues[ i - 1 ] = calculateValues[i];
                        calculateValues[i] = tmp;

                    }
                }
            }
            return calculateValues;
        }

    }
}