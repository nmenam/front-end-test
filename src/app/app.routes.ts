import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DictComponent } from './components/dict/dict.component';
import { ArrayComponent } from './components/array/array.component';


const APP_ROUTES: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'array', component: ArrayComponent},
    {path: 'dict', component: DictComponent},
    {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
