import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    private apiRoot = 'http://168.232.165.184/prueba/';

    constructor(private http: HttpClient) {
    }

    public getData(endpoint: string) {
        return this.http.get(`${this.apiRoot}${endpoint}`);
    }
}

